<?php


class Tree
{
    private $treeArray = [];
    private $freeLeaves = [];
    private $maxWeight;

    const NODE = 'nodes';
    const LEAF = 'lists';

    function __construct($treeArray = [], $maxWeight)
    {
        $this->treeArray = $treeArray;
        $this->maxWeight = $maxWeight;
        $this->parseTree();
    }

    private function parseTree()
    {
        $this->treeArray = $this->parseNode($this->treeArray);
    }

    private function parseNode(Array $tree)
    {
        foreach ($tree as $key => $value) {
            $tree[$key][self::LEAF] = $this->deleteLeaves($tree[$key][self::LEAF]);
            if ($tree[$key][self::NODE]) {
                $tree[$key][self::NODE] = $this->parseNode($tree[$key][self::NODE]);
            }
        }

        return $tree;
    }

    private function quick_sort($leaves = [])
    {
        $length = count($leaves);
        if ($length <= 1){
            return $leaves;
        }
        else{
            $pivot = $leaves[0];
            $left = $right = array();
            for ($i = 1; $i < count($leaves); $i++) {
                if ($leaves[$i] < $pivot) {
                    $left[] = $leaves[$i];
                }
                else {
                    $right[] = $leaves[$i];
                }
            }
            return array_merge($this->quick_sort($left), array($pivot), $this->quick_sort($right));
        }
    }
    
    private function deleteLeaves($leaves = [])
    {
        $leaves = $this->quick_sort(array_merge($this->freeLeaves, $leaves));
        $totalWeight = array_shift($leaves);


        if ($totalWeight > $this->maxWeight) {
            $this->freeLeaves = $leaves;
            return [];
        }

        $newLeaves = [$totalWeight];
        $this->freeLeaves = [];

        for ($i = 0; $i < count($leaves); $i++) {
            if($totalWeight + $leaves[$i] <= $this->maxWeight) {
                $newLeaves[] = $leaves[$i];
                $totalWeight += $leaves[$i];
            }
            else {
                $this->freeLeaves[] = $leaves[$i];
            }
        }

        return $newLeaves;
    }

    public function getTree()
    {
        return $this->treeArray;
    }

    public function getFreeLeaves()
    {
        return $this->freeLeaves;
    }
}
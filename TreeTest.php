<?php

include('Tree.php');

class TreeTest extends \PHPUnit\Framework\TestCase
{
    public $max = 5;
    public $tree = [
        [
            'lists' => [1,2,5,4],
            'nodes'  => [
                [
                    'lists' => [7,3,1],
                    'nodes'  => [
                        [
                            'lists' => [8,3,22],
                            'nodes'  => [
                                [
                                    'lists' => [1,4,3,1],
                                    'nodes'  => [
                                        [
                                            'lists' => [12,8,2,6],
                                            'nodes'  => NULL
                                        ],
                                        [
                                            'lists' => [15,9,2,4],
                                            'nodes'  => NULL
                                        ],
                                        [
                                            'lists' => [22,13,51],
                                            'nodes'  => NULL
                                        ],
                                        [
                                            'lists' => [62,7,2],
                                            'nodes'  => NULL
                                        ],
                                    ]
                                ],
                                [
                                    'lists' => [2],
                                    'nodes'  => [
                                        [
                                            'lists' => [2,3,9,23],
                                            'nodes'  => NULL
                                        ],
                                    ]
                                ],
                            ]
                        ],
                        [
                            'lists' => [8,13,51],
                            'nodes'  => [
                                [
                                    'lists' => [92,63,1,2],
                                    'nodes'  => NULL
                                ],
                                [
                                    'lists' => [2,3,1],
                                    'nodes'  => NULL
                                ],
                                [
                                    'lists' => [9,10,14,1,12],
                                    'nodes'  => NULL
                                ],
                                [
                                    'lists' => [1],
                                    'nodes'  => NULL
                                ],
                            ]
                        ],
                    ]
                ],
                [
                    'lists' => [7,5,1],
                    'nodes'  => NULL
                ],
            ]
        ]
    ];

    function testTrue() {
        $treeObject = new Tree($this->tree, $this->max);
        $sortTree = $treeObject->getTree();
        $this->treeParse($sortTree);
    }

    private function treeParse($tree)
    {
        foreach ($tree as $node) {
            $this->assertLessThanOrEqual($this->max, $this->sumArray($node['lists']));
            if($node['nodes']) {
                $this->treeParse($node['nodes']);
            }
        }
    }

    private function sumArray(Array $lists)
    {
        $total = 0;
        foreach ($lists as $list) {
            $total += $list;
        }
        return $total;
    }
}